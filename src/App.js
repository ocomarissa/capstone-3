import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { UserProvider } from './UserContext';
import NavBar from './components/NavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Footer from './components/Footer';
import Error from './pages/Error';
import Products from './pages/Products';
import Cart from './pages/Cart';
import OrderHistory from './pages/OrderHistory';
import Highlights from './components/Highlights';
import SpecificProducts from './pages/SpecificProduct';

export default function App() {
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    })

    const unsetUser = () => {
        localStorage.clear()
        setUser({
            id: null,
            isAdmin: null
        })
    }
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (typeof data._id !== "undefined") {
                    setUser({
                        id: data._id,
                        isAdmin: data.isAdmin
                    })
                } else {
                    setUser({
                        id: null,
                        isAdmin: null
                    })
                }
            })

    }, [])

    return (
        <UserProvider value={{ user, setUser, unsetUser }}>
            <Router>
                <section className="navbar-nav nav-link">
                    <NavBar />
                </section>
                <section id="main-body">
                    <div>
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route exact path="/register" component={Register} />
                            <Route exact path="/login" component={Login} />
                            <Route exact path="/products" component={Products} />
                            <Route exact path="/products/:productId" component={SpecificProducts} />
                            <Route exact path="/cart" component={Cart} />
                            <Route exact path="/orders" component={OrderHistory} />
                            <Route component={Error} />
                        </Switch>
                    </div>
                </section>
                {/* <section id="second-body">
                        <Highlights/>
                </section> */}
                {/* <section id="footer">
                        <Footer />
                </section> */}
               
            </Router>
        </UserProvider>
    );
}