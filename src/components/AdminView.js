import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props) {

    const { productsData, fetchData } = props;
    const [productId, setProductId] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [imageUrl, setImageUrl] = useState("");
    const [countInStock, setCountinStock] = useState(0);
    const [price, setPrice] = useState(0);
    const [products, setProducts] = useState([]);
    const [showEdit, setShowEdit] = useState(false);
    const [showAdd, setShowAdd] = useState(false);

    const openAdd = () => setShowAdd(true)
    const closeAdd = () => setShowAdd(false)

    const closeEdit = () => {
        setShowEdit(false)
        setName("")
        setDescription("")
        setPrice()
        setImageUrl("")
        setCountinStock(0)
    }
    const openEdit = (productId) => {
        console.log(productId)
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setProductId(data._id)
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
                setImageUrl(data.imageUrl)
                setCountinStock(data.countInStock)
            })
        setShowEdit(true)
    }

    useEffect(() => {
        console.log(props)
        const productsArr = productsData.map(product => {
            return (
                <tr key={product._id}>
                    {/* <td>{product.imageUrl}</td> */}
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    <td>{product.price}</td>
                    {/* <td>{product.countInStock}</td> */}
                    <td>
                        {product.isActive
                            ? <span>Available</span>
                            : <span>Unavailable</span>
                        }
                    </td>
                    <td>
                        <Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
                        {product.isActive
                            ? <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
                            : <Button variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
                        }
                    </td>
                </tr>
            )
        })

        setProducts(productsArr)

    }, [productsData])



    const addProduct = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/products`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                imageUrl: imageUrl,
                countInStock: countInStock
            })

        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    fetchData()
                    Swal.fire({
                        title: "Success",
                        icon: "success",
                        text: "Product has been added"
                    })
                    setName("")
                    setDescription("")
                    setPrice("")
                    setImageUrl("")
                    setCountinStock("")
                    closeAdd()

                } else {
                    Swal.fire({
                        title: "Failed",
                        icon: "error",
                        text: "Please try again."
                    })
                    closeAdd()
                }

            })
    }

    const editProduct = (e, productId) => {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                imageUrl: imageUrl,
                countInStock: countInStock
            })

        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    fetchData()
                    Swal.fire({
                        title: "Success",
                        icon: "success",
                        text: "Product has been updated"
                    })
                    closeEdit();
                } else {
                    Swal.fire({
                        title: "Failed",
                        icon: "error",
                        text: "Please try again."
                    })
                }

            })
    }

    const archiveToggle = (productId, isActive) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    fetchData()
                    Swal.fire({
                        title: "Success",
                        icon: "success",
                        text: "Product has been updated"
                    })
                } else {
                    fetchData()
                    Swal.fire({
                        title: "Failed",
                        icon: "error",
                        text: "Please try again."
                    })
                }
            })
    }

    return (
        <>
            <div className="text-center my-4">
                <h2>Admin Dashboard</h2>
                <div className="d-flex justify-content-center">
                    <Button variant="primary" onClick={openAdd}>Add New Product</Button>
                </div>
            </div>
            <Table striped bordered hover responsive>
                <thead className="bg-dark text-white">
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        {/* <th>ImageUrl</th>
                        <th>Stocks</th> */}
                        <th>Availability</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {products}
                </tbody>
            </Table>
            {/* Edit Modal */}
            <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editProduct(e, productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
                        </Form.Group>
                        <Form.Group controlId="productDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
                        </Form.Group>
                        <Form.Group controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
            {/* Add Modal */}
            <Modal show={showAdd} onHide={closeAdd}>
                <Form onSubmit={e => addProduct(e, productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
                        </Form.Group>
                        <Form.Group controlId="productDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
                        </Form.Group>
                        <Form.Group controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
                        </Form.Group>
                        <Form.Group controlId="imageUrl">
                            <Form.Label>URL</Form.Label>
                            <Form.Control type="text" value={imageUrl} onChange={e => setImageUrl(e.target.value)} required />
                        </Form.Group>
                        <Form.Group controlId="countInStock">
                            <Form.Label>Stocks</Form.Label>
                            <Form.Control type="number" value={countInStock} onChange={e => setCountinStock(e.target.value)} required />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}