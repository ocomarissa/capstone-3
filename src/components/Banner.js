import { Link } from 'react-router-dom'

export default function Banner({ bannerProps }) {

    const { title, content, destination, label } = bannerProps;

    return (
        <>
            <div className="entries">
                <h1>{title}</h1>
            </div>
            <div className="home">
                <h1>{content}</h1>
                <Link className="btn" to={destination}>{label}</Link>
            </div>
        </>
    )
}