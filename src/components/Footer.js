import React from "react";


export default function Footer () { 
    return(
        <>
            <footer class="footer section">
                <div class="container">
                    <ul class="footer-section">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#projects">Projects</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                    <h3 class="text-center">Copyright @2021 | Designed With by <a href="#">Marissa Mesa</a></h3>
                </div>
            </footer>
        </>
    )
}

