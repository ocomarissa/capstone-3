import { useState, useEffect } from 'react';
import { Carousel, Card, Link } from 'react-bootstrap';

export default function Highlights() {
   const [data, setData] = useState([]);
    const fetchData = async () => {
        try {
            let response = await fetch(`${process.env.REACT_APP_API_URL}/products/`)
            let info = await response.json();
            setData(info);
        } catch (err) {
            alert(err)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

      return (
        <div className="home">
            <h2> Our Bestsellers </h2>
            <Carousel>
                {data.map(product => (
                    <Carousel.Item key={product.id}>
                        <div className="entries2">
                            <Card style={{ width: '15rem' }} >
                                <Card.Img variant="top" src={product.imageUrl} />
                                <Card.Body className="entries2">
                                    <Card.Title className="text-center">
                                        <h2>{product.name}</h2>
                                    </Card.Title>
                                    <Card.Text>
                                        <h4>Price: Php {product.price}</h4>
                                        {/* <p>Stock: {countInStock}</p> */}
                                    </Card.Text>
                                    {/* <Link className="btn btn-primary btn-block" to={`/products/${_id}`}>Details</Link> */}
                                </Card.Body>
                            </Card>

                        </div>
                    </Carousel.Item>

                ))
                }
            </Carousel>
        </div>
    );
}




