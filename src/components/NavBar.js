import { useContext, useState } from 'react';
import { Navbar, Nav } from "react-bootstrap";
import { Link, useHistory } from 'react-router-dom';
import UserContext from "../UserContext"


export default function NavBar() {
    const { user, unsetUser } = useContext(UserContext)
    const history = useHistory()
    // const [cartItems, setCartItems] = useState(0)
    // const [orders, setOrders] = useState(0)

    const logout = () => {
        unsetUser()
        history.push('/login')
    }
    let rightNav = (!user.id) 
        ?(
            <>
            <Link className="nav-link" to="/register"> Register </Link>
            <Link className="nav-link" to="/login"> Log In </Link>
            </>
        ) 
        :(
            <>
            {/* <Link className="nav-link" to="/cart"> Cart({cartItems}) </Link>
            <Link className="nav-link" to="/orders"> Orders({orders}) </Link> */}
            <Link className="nav-link" to="/cart"> Cart </Link>
            <Link className="nav-link" to="/orders"> Orders </Link>
            <Nav.Link onClick={logout}> Log Out </Nav.Link>
            </>
        )

    return (
        <>
            <Nav className="justify-content-end" activeKey="/home">
                <Nav.Item>
                    <Link className="nav-link" to="/">Home</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link className="nav-link" to="/products">Products</Link>
                </Nav.Item>
                <Nav.Item>
                    {rightNav}
                </Nav.Item>
            </Nav>
        </>
    );
}



