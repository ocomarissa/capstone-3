
import { Card, Container, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Product({ productProp }) {
    const { _id, name, price, imageUrl} = productProp;
    return (
        <div className="entries2">
            <Card style={{ width: '20rem' }} >
                <Card.Img variant="top" src={imageUrl} />
                <Card.Body className="entries2">
                    <Card.Title className="text-center">
                        <h2>{name}</h2>
                    </Card.Title>
                    <Card.Text>
                        <h4>Price: Php {price}</h4>
                        {/* <p>Stock: {countInStock}</p> */}
                    </Card.Text>
                    <Link className="btn btn-primary btn-block" to={`/products/${_id}`}>Details</Link>
                </Card.Body>
            </Card>
            </div>
    );
}
