import { useState, useEffect } from 'react'
import Product from "./Products";


export default function UserView({productsData}) {
    const [products, setProducts] = useState([])

    useEffect(() => {
        const productArr = productsData.map(product => {
            if (product.isActive === true) {
                return (
                    <Product productProp={product} key={product._id} />
                )
            } else {
                return null;
            }
        });
        setProducts(productArr)
    }, [productsData])

    return (
        <>
            <h1>{products}</h1>
        </>
    );
}