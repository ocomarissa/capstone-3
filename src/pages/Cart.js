import React, { useState, useEffect, useContext } from 'react';
import { Container, Table, Form } from 'react-bootstrap'
import { Link, Redirect  } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function MyCart() {

    const { user } = useContext(UserContext);
    const [total, setTotal] = useState(0)
    const [cart, setCart] = useState([])
    const [tableRows, setTableRows] = useState([])
    const [gtorder, setGtorder] = useState("false")

    useEffect(() => {
        if (localStorage.getItem('cart')) {
            setCart(JSON.parse(localStorage.getItem('cart')))
        }
    }, [])

    const enterQuantity = (productId, quantity) => {
        let newCart = [...cart]
        if ((quantity === '') || (quantity < 0)){
            quantity = 0
        }
        for (let i = 0; i < cart.length; i++) {
            if (newCart[i].productId === productId) {
                    newCart[i].quantity = parseFloat(quantity)
                    newCart[i].subtotal = newCart[i].price * newCart[i].quantity
            }
        }
        setCart(newCart)
        localStorage.setItem('cart', JSON.stringify(newCart))
    }

    const increaseDecrease = (productId, plusMinus) => {
        let newCart = [...cart]
        for (let i = 0; i < newCart.length; i++) {
            if (newCart[i].productId === productId) {
                if (plusMinus === "+") {
                    newCart[i].quantity += 1
                    newCart[i].subtotal = newCart[i].price * newCart[i].quantity

                } else if (plusMinus === "-") {
                    if ( newCart[i].quantity > 0 ) {
                        newCart[i].quantity -= 1
                        newCart[i].subtotal = newCart[i].price * newCart[i].quantity
                    }
                }
            }
        }
        setCart(newCart)
        localStorage.setItem('cart', JSON.stringify(newCart))
    }


    const deleteItem = (productId) => {
        let newCart = [...cart]
        newCart.splice ([newCart.indexOf(productId)], 1)
        setCart(newCart)
        localStorage.setItem('cart', JSON.stringify(newCart))
    }


    useEffect(() => {
        let cartItems = cart.map ((item, index) => {
            return (
                <tr key={item.productId}>
                    <td>
                        <Link to={`/products/${item.productId}`}>{item.name}</Link>
                    </td>
                    <td>₱{item.price}</td>
                    <td>
                        <div className="entries">
                            <button class="btn" onClick={() => increaseDecrease(item.productId,"-")}> - </button>
                            <Form>
                                <Form.Control type="number" value={item.quantity} onChange={e => enterQuantity(item.productId, e.target.value)} required />
                            </Form>
                            <button class="btn" onClick={() => increaseDecrease(item.productId, "+")}> + </button>
                        </div>
                    </td>
                    <td>₱{item.subtotal}</td>
                    <td className="text-center">
                        <button class="btn" onClick={() => deleteItem(item.productId)}> Delete </button>
                    </td>
                </tr>
            )
        })
        setTableRows(cartItems)
        let newTotal = 0

        cart.forEach((item) => {
            newTotal += item.subtotal
        })

        setTotal(newTotal)
    }, [cart])

   
    const checkoutOrder = () => {
        const checkoutCart = cart.map((item) => {
            return {
                productId: item.productId,
                productName: item.name,
                quantity: item.quantity,
            }
        })
        fetch(`${process.env.REACT_APP_API_URL}/orders/`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                products: checkoutCart,
                totalAmount: total
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    Swal.fire({
                        title: "Yey! Order is on the way.",
                        icon: 'success',
                        text: "Sweetness Overload!"
                    })
                    localStorage.removeItem('cart')
                    setGtorder(true)
                } else {
                    alert("Something went wrong. Order was NOT placed.")
                }
            })

    }
    return (
        <>
            <Container className="entries2">
                {(user.isAdmin === true)
                    ? <Redirect to="/products"/>
                    : gtorder === true
                        ? <Redirect to="/orders" />
                        :
                        cart.length <= 0
                            ?
                            <div>
                                <h3 className="text-center">Your cart is empty! <Link to="/products"> Start shopping. </Link></h3>
                            </div>
                            :
                            <Container>
                                <h2 className="text-center my-4">Hello User, your Shopping Cart</h2>
                                <Table striped bordered hover responsive>
                                    <thead className="bg-danger text-white align-center">
                                        <tr>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Subtotal</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {tableRows}
                                        <tr>
                                            <td colSpan="3">

                                            </td>
                                            <td colSpan="2">
                                                <h3>Total: ₱{total}</h3>
                                            </td>
                                        </tr>
                                    </tbody>

                                </Table>

                                <div id="entries">
                                    <button class="btn" onClick={() => checkoutOrder()}> Checkout </button>
                                </div>

                            </Container>
                }
            </Container>
        </>


       
    )
}