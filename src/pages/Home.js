import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
    const data = {
        title: "Welcome to CupcakeWorld!",
        content: "You can't buy HAPPINESS, but you can but CUPCAKES and that's kind of the same thing.'",
        destination: "/products",
        label: "Order Now!"
    }
    return (
        <>
            <Banner bannerProps={data} />
            <Highlights/>
        </>
    );
}