// A Login Page that must redirect the user to the either the home page or the products catalog once successfully authenticated.

import { useState, useContext } from 'react';
import { Form, Container, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const { user, setUser } = useContext(UserContext)

    const loginUser = (e) => {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {
                if (typeof data.access !== "undefined") {
                    localStorage.setItem('token', data.access)
                    retrieveUserDetails(data.access)
                } else {
                    Swal.fire({
                        title: "Authentication failed",
                        icon: "error",
                        text: "Check your login details and try again."
                    })
                }
            })
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }

    if (user.id != null) {
        return <Redirect to="/" />
    }
    return (
        <>
        <Container>
            <Form className="mt-3" onSubmit={e => loginUser(e)}>
                <Form.Group>
                    <Form.Label>Email Address:</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required />
                </Form.Group>

                <Button variant="primary" type="submit">Login</Button>

            </Form>
        </Container>
        </>
    )
}