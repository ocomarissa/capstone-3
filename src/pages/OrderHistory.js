// An Order History page where the currently logged -in user can see all records of their previously - placed orders.

import { useState, useEffect} from 'react';
import { Container, Table} from 'react-bootstrap';

export default function OrderHistory() {

    const [orders, setOrders] = useState([]);
    const [product, setProducts] = useState([]);



    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/myPurchases`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                const ordersArr = data.map(data => {
                    return (
                        <tr key={data._id}>
                            <td>{data._id}</td>
                            <td>{data.purchasedOn}</td>
                            <td>{data.totalAmount}</td>
                            <td>Product</td>
                        </tr>
                    )
                })
                setOrders(ordersArr)

            })
    }, [])

    return (

        <Container className="entries2">
            <h1 className="text-center py-3">Order History</h1>
            <Table striped bordered hover responsive>
                <thead className='bg-danger text-white'>
                    <tr>
                        <th>Order ID</th>
                        <th>Purchased On</th>
                        <th>Total Amount</th>
                        <td>Products</td>
                    </tr>
                </thead>
                <tbody>
                    {orders}
                </tbody>
            </Table>
        </Container>

    )
}





