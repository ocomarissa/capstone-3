// A Product Catalog page that must show all products currently available(unavailable / inactive products must not be shown)
// and allow users to visit individual product pages

import { useState, useEffect, useContext } from 'react'
import { Container } from "react-bootstrap";
import AdminView from "../components/AdminView";
import UserView from "../components/UserView";
import UserContext from '../UserContext';


export default function Products() {

  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([])

  const fetchData = async() => {
    try{
      let response = await fetch(`${process.env.REACT_APP_API_URL}/products/`)
      let data = await response.json();
      setProducts(data);
    }catch(err){
      alert(err)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <>
    <Container>
      {(user.isAdmin === true)
        ? <AdminView productsData={products} fetchData={fetchData}/>
        : <UserView productsData={products}/>
      }
    </Container>
    </>
  );
}