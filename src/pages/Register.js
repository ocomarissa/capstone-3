// Registration Page with proper form validation(all fields must be filled and passwords must match)
// that must redirect the user to the login page once successfully submitted.

import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [address, setAddress] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    const { user } = useContext(UserContext)
    const history = useHistory();
    const [registerButton, setRegisterButton] = useState(false);


    function registerUser(e) {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(res => res.json())
            .then(data => {

                if (data === true) {
                    Swal.fire({
                        title: 'Duplicate email found.',
                        icon: 'error',
                        text: 'Please login instead.'
                    })
                } else {
                    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            mobileNo: mobileNo,
                            address: address,
                            password: password
                        })
                    })
                        .then(res => res.json())
                        .then(data => {
                            if (data === true) {
                                Swal.fire({
                                    title: 'Registration successful.',
                                    icon: 'success',
                                    text: 'Enjoy Shopping!'
                                })

                                history.push('/login')
                            } else {
                                Swal.fire({
                                    title: 'Something went wrong.',
                                    icon: 'error',
                                    text: 'Please try again.'
                                })
                            }
                        })
                }

            })
    }
    useEffect(() => {
        if ((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)) {
            setRegisterButton(true)
        } else {
            setRegisterButton(false)
        }
    }, [email, password, verifyPassword])

    if (user.id != null) {
        return <Redirect to="/" />
    }
    return (
        <>
        <Container>
            <Form className="mt-3" onSubmit={(e) => registerUser(e)}>
                <Form.Group>
                    <Form.Label>First Name:</Form.Label>
                    <Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Last Name:</Form.Label>
                    <Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)} required />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Email Address:</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Mobile Number:</Form.Label>
                    <Form.Control type="text" placeholder="Enter mobile number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Address:</Form.Label>
                    <Form.Control type="text" placeholder="Enter address" value={address} onChange={e => setAddress(e.target.value)} required />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Verify Password:</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required />
                </Form.Group>

                {registerButton 
                    ?<Button variant="primary" type="submit">Submit</Button>
                    :<Button variant="primary" type="submit" disabled>Submit</Button>
                }
            </Form>
        </Container>
        </>


    )
}