import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Form} from 'react-bootstrap';
import { Link, useParams} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function SpecificProducts() {
    const {user} = useContext(UserContext);
    const { productId } = useParams();
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [imageUrl, setImageUrl] = useState("");
    // const [countInStock, setCountInStock] = useState(0);
    const [quantity, setQuantity] = useState(1);
    const [subTotal, setSubTotal] = useState("");
    const [id, setId] = useState("");
    const [cart, setCart] = useState();


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(res => res.json())
            .then(data => {
                setId(data._id)
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
                setImageUrl(data.imageUrl)
                setSubTotal(data.price)
                // setCountInStock(data.countInStock)
                
            });
    }, []);

    useEffect(() => {
        setSubTotal(quantity*price)
    }, [quantity])

    const increment = () => {
        setQuantity(prevCount => prevCount+1);
    }

    const decrement = () => {
        if(quantity>1){
        setQuantity(quantity - 1)
        }
    }

    const addToCart = () => {
        let inCart, index , info = false;
        let cart = []
        if (localStorage.getItem('cart')) {
            cart = JSON.parse(localStorage.getItem('cart'))
        }
        for (let i = 0; i < cart.length; i++) {
            if (cart[i].productId === id) {
                inCart = true
                index = i
            }
        }
        if (inCart) {
            cart[index].quantity += quantity
            cart[index].subtotal = cart[index].price * cart[index].quantity
        }else{
            cart.push({
                'productId': id,
                'name': name,
                'price': price,
                'quantity': quantity,
                'subtotal': subTotal
             })
        }
        localStorage.setItem('cart', JSON.stringify(cart))
        if(quantity>1){
            info = `${quantity} items added to cart.`
        }else{
            info = '1 item added to cart.'
        }
        Swal.fire({
            title: info,
            icon: 'success',
            text: "Sweetness Overload!"
        })
    }

    let cardFooter = (user.id !==null)
        ?
        (
            <>
                <div className="entries">
                    <button class="btn" onClick={() => decrement()}> - </button>
                    <Form>
                        <Form.Control type="number" placeholder="Enter Quantity" value={quantity} onChange={e => setQuantity(e.target.value)} required />
                    </Form>
                    <button class="btn" onClick={() => increment()}> + </button>
                </div>
                <Button variant="primary" block onClick={() => addToCart(productId)}>Add to Cart</Button>
            </>
        )
        :
        (
            <>
                <Link className="btn btn-danger btn-block" to="/login"> Log in to Shop </Link>
            </>
        )

    return (
                <Card style={{ width: '30rem' }} className="entries2" >
                <Card.Img variant="top" src={imageUrl} />
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Text>
                        <p>{description}</p>
                        <h2>Price: Php {price}</h2>
                        <div>Total : {subTotal}</div>
                        {/* <p>Stock: {countInStock}</p> */}
                    </Card.Text>
                </Card.Body>
                <Card.Footer>
                    {cardFooter}
                </Card.Footer>
            </Card>
    )
}

